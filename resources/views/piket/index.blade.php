<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap-4.1.1/dist/css/bootstrap.min.css">
	<style>
		h5{
			text-align: center;
			padding: 20px;
		}
 
		table {
			text-align: center;
		}

		th {
			padding:10px;
			text-align: center;
		}

		.table {
			border : 1px solid #000;
		}
	</style>
    <title>Jadwal piket</title>
  </head>
  <body style="background-color: #ccc;">
    <h1 style="margin: 50px; text-align: center;">DAFTAR PIKET HARIAN</h1>

	<div class="container" style="background-color: white">
		<div class="row">
			<div class="col-6">
			<h5>NAMA SANTRI</h5>
				<table class="table">
					<thead>
						<tr>
							<th>Group</th>
							<th>Nama Santri</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>A</td>
							<td>adi , firman</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-6">
				<h5>KET AREA</h5>
				<table class="table">
					<thead>
						<tr>
							<th>Area</th>
							<th>Keterangan</th>
						</tr>
					</thead>
					<tbody>
						@foreach($places as $place)
						<tr>
							<td>{{ $place->code }}</td>
							<td>{{ $place->description }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="container" style="background-color: white; margin-top: 20px;">
	<h5>JADWAL PIKET</h5>
		<table class="table" border="1">
			<tr>
				<th>AREA</th>
				<th>SENIN</th>
				<th>SELASA</th>
				<th>RABU</th>
				<th>KAMIS</th>
				<th>SABTU</th>
			</tr>
			<tr>
				<td>nama area</td>
				<td>nama santri</td>
				<td>nama santri</td>
				<td>nama santri</td>
				<td>nama santri</td>
				<td>nama santri</td>
			</tr>
		</table>
	</div>

    <script src="bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
  </body>
</html>