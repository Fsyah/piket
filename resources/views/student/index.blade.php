<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap-4.1.1/dist/css/bootstrap.min.css">
	<style>
		h5{
			text-align: center;
			padding: 10px;
		}
 
		table {
			text-align: center;
		}

		th {
			padding:10px;
			text-align: center;
		}

		.table {
			border : 1px solid #000;
		}
	</style>
    <title>Jadwal piket</title>
  </head>
  <body style="background-color: #ccc;">
    
  <div class="container" style="background-color: white">
		<div class="row" >
			<div class="container" style="margin:50px;">
			@if (\Session::has('success'))
				<div class="alert alert-success">
					<p>{{ \Session::get('success') }}</p>
				</div><br />
			@endif 
				<div class="col-6">
					<form  method="post" action="{{ route('student.store') }}">
						{{ csrf_field() }}
						<div class="index"  >
							<label >Nama</label>
							<input type="text" name="name">
						</div>
						
						<div class="form-group">
							<input type="submit" style="background:blue" class="btn btn-primary" value="Save">
						</div>
					</form>
				</div>
			


				<div class="col-6">
					<h5>Nama</h5>
					<table class="table">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
							</tr>
						</thead>
						<tbody>
							@php ($no = 1) @endphp
							@foreach($students as $student)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{ $student->name }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{!! $students->links() !!}
				</div>
			</div>
		</div>
	</div>

    <script src="bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
  </body>
</html>