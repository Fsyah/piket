<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';
    
    protected $fillable = ['group'];


    public function student()
    {
        return $this->hasMany(Student::class); 
    }
}
 