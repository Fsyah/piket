<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    protected $fillable = ['name', 'group_id'];
    
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
  